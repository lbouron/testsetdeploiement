<?php

// Connexion à la base de données
include 'bdd/connect.php';

// Partie affichage du contact
$sql = "SELECT idContact, nomContact, prenomContact, emailContact, telContact
        FROM CONTACTS
        WHERE idContact = ?";

$query = $pdo->prepare($sql);
$query->execute([$_GET['idContact']]);

$contact = $query->fetch(PDO::FETCH_ASSOC);

// if($contact == false) {
//     header('Location: index.php');
//     exit;
// }

// Modifier un contact
$erreur = [];

if($_POST) {
    $nomcontact = $_POST['nom'];
    $prenomcontact = $_POST['prenom'];
    $mailcontact = $_POST['email'];
    $telcontact = $_POST['tel'];

    if(empty($nomcontact)) {
        $erreur[] = "Il faut un nom.";
    }

    if(empty($prenomcontact)) {
        $erreur[] = "Il faut un prénom.";
    }

    if(empty($mailcontact)) {
        $erreur[] = "Il faut un email.";
    }

    if(empty($telcontact)) {
        $erreur[] = "Il faut un numéro de téléphone.";
    }

    if(count($erreur) == 0) {
        $sql = "UPDATE CONTACTS
                SET nomContact = ?,
                    prenomContact = ?,
                    emailContact = ?,
                    telContact = ?
                WHERE idContact = ?";

        $query = $pdo->prepare($sql);
        $query->execute([
                        $nomcontact,
                        $prenomcontact,
                        $mailcontact,
                        $telcontact,
                        $idcontact
            ]);
        
        header('Location: index.php');
        exit;
    } else {
        $erreur[] = "Il faut remplir tous les champs.";
    }
}

// Include du template
$title = "Éditer le contact";
$template = "views/update.phtml";

include "template/template.php";