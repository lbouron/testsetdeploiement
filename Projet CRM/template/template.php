<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <!-- CSS only (Boostrap) -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <title><?= $title ?></title>
    </head>

    <body>
        <header>
            <nav class="navbar py-lg-3 px-lg-6 navbar-light" style="background-color: #4FD1C5;">
                <div class="container-fluid">
                    <form class="container-fluid justify-content-start" style="margin-bottom: 0;">
                        <a href="add.php" class="btn btn-outline-dark">Ajouter un contact</a>
                        <!-- <a href="update.php" class="btn btn-outline-dark">Modifier un contact</a>
                        <a href="delete.php" class="btn btn-outline-dark">Supprimer un contact</a> -->
                    </form>
                </div>
            </nav>
        </header>

        <main>
            <?php include $template ?>
        </main>

        <footer>
            <!-- Vide -->
        </footer>
        <script type="text/javascript" src="script.js"></script>
    </body>
</html>