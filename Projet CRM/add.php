<?php

namespace App;

// Connexion à la base de données
include 'bdd/connect.php';

// Créer un nouveau contact
$erreur = [];
$message = null;

if($_POST) {
    $nomcontact = $_POST['nom'];
    $prenomcontact = $_POST['prenom'];
    $mailcontact = $_POST['email'];
    $telcontact = $_POST['tel'];

    // var_dump($_POST);

    if(!empty($nomcontact) || !empty($prenomcontact) || !empty($mailcontact) || !empty($telcontact)) {

        if(strlen($nomcontact) < 2 || strlen($nomcontact) > 50) {
            $erreur[] = "Le nom doit contenir entre 2 et 50 caractères.";
        }

        if(strlen($prenomcontact) < 2 || strlen($prenomcontact) > 50) {
            $erreur[] = "Le prénom doit contenir entre 2 et 50 caractères.";
        }

        if(stristr($mailcontact, '@') === FALSE) {
            $erreur[] = "L'adresse email nécessite un @.";
        }

        if(!preg_match(" /^[^\W][a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/ ", $mailcontact)) {
            $erreur[] = "L'adresse email est incorrecte.";
        }

        if(!filter_var($mailcontact, FILTER_VALIDATE_EMAIL)) {
            $erreur[] = "L'adresse email est incorrecte.";
        }

        // if(preg_match("", $telcontact)) {
        //     $erreur[] = "Le numéro de téléphone ne doit pas contenir de lettres ou de caractères spéciaux.";
        // }

        if(strlen($telcontact) > 10 || strlen($telcontact) < 10) {
            $erreur[] = "Le numéro de téléphone doit avoir 10 chiffres.";
        }

        if(count($erreur) == 0) {
            // Requête d'insertion dans la table CONTACTS
            $sql = "INSERT INTO CONTACTS (nomContact, prenomContact, emailContact, telContact) VALUES
                    (?, ?, ?, ?)";
            
            $query = $pdo->prepare($sql);
            $query->execute([
                            $nomcontact,
                            $prenomcontact,
                            $mailcontact,
                            $telcontact
                ]);

            $message = "Le contact a bien été créé.";
        } else {
            $erreur[] = "Tous les champs doivent être remplis.";
        }
    }
}

// Include du template
$title = "Créer un contact";
$template = "views/create.phtml";

include "template/template.php";