-- Création et utilisation de la BDD
CREATE DATABASE projetcrm;
USE projetcrm;

-- Création de la table CONTACTS
CREATE TABLE CONTACTS (
    idContact INT(10) NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nomContact VARCHAR(255) NOT NULL,
    prenomContact VARCHAR(255) NOT NULL,
    emailContact VARCHAR(255) NOT NULL,
    telContact VARCHAR(255) NOT NULL
);