<?php

try {
    $host = 'localhost';
    $dbname = 'projetcrm';
    $username = 'root';
    $password = '';

    // Connexion
    $pdo = new PDO("mysql:host=$host;port=3306;dbname=$dbname","$username","$password");
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET NAMES UTF8');
} catch (PDOException $e) {
    echo 'La connexion a échoué.<br>';
    echo '<br>Code erreur : [' , $e->getCode(), '] ';
    echo '<br>Message erreur : '. $e->getMessage();
    echo '<br>Ligne de l\'erreur : '. $e->getLine();
    echo '<br>Fichier contenant l\'erreur : '. $e->getFile();
}