<?php

// Connexion à la base de données
include 'bdd/connect.php';

// Supprimer un contact
$sql = "DELETE
        FROM CONTACTS
        WHERE idContact = ?";

$query = $pdo->prepare($sql);
$query->execute([$_GET['idContact']]);

header('Location: index.php');
exit;