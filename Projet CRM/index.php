<?php

// Connexion à la BDD
include 'bdd/connect.php';

// Requête SQL pour afficher tous les contacts
$sql = "SELECT idContact, nomContact, prenomContact, emailContact, telContact
        FROM CONTACTS";

$query = $pdo->prepare($sql);
$query->execute();

$contacts = $query->fetchALL(PDO::FETCH_ASSOC);

// Include du Template
$title = "Accueil";
$template = "views/index.phtml";

include 'template/template.php';